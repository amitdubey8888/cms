import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../providers/common.service';


@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {

  private studentDetails = {
      name: 'Jay Rana',
      fatherName: 'Ram Avatar Singh Rana',
      motherName: 'Kamlesh Devi',
      dob: new Date(),
      mobile: '+918385803337',
      email: 'jkrana008@gmail.com',
      class: 'B. Sc.',
      year: '2nd',
      rollNumber: '1234',
      admissionDate: new Date(),
      registration: '112342412'
    };

  constructor(
    private common: CommonService
  ) {
    this.common.display.navbar = true;
    this.common.display.footer = true;
    this.common.display.copyright = true;
  }

  ngOnInit() {
  }

}

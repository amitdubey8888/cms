import { Component, OnInit } from '@angular/core';


import { CommonService } from '../../providers/common.service';


@Component({
  selector: 'app-fee',
  templateUrl: './fee.component.html',
  styleUrls: ['./fee.component.css']
})
export class FeeComponent implements OnInit {

  constructor(
    private common: CommonService
  ) {
    this.common.display.navbar = true;
    this.common.display.footer = true;
    this.common.display.copyright = true;
  }

  ngOnInit() {
  }

}

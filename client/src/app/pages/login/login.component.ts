import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { CommonService } from '../../providers/common.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;

  constructor(
    private router:Router,
    private common: CommonService
  ) { }

  ngOnInit() {
  }

  login(){
    this.router.navigate(['/home']);
  }

}

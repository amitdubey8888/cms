import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from "@angular/forms";
declare let swal: any;

import { CommonService } from '../../providers/common.service';
import { ApiService } from '../../providers/api.service';
import { Course } from '../../models/course';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  courses: Course[];
  course: Course = {
    _id: '',
    name: '',
    duration: 0,
    fee: 0,
    seats: 0,
    firstYearFilledSeats: 0
  };
  modal: any;

  constructor(
    private common: CommonService,
    private modalService: NgbModal,
    private api: ApiService
  ) {
    this.common.display.navbar = true;
    this.common.display.footer = true;
    this.common.display.copyright = true;
    this.getCourses();

  }


  ngOnInit() {
  }

  // Get Courses
  getCourses() {
    this.common.display.loader = true;
    this.api.getCourse()
      .subscribe(courses => {
        this.courses = courses;
        this.common.display.loader = false;
      });
  }

  /* Add Course */
  addCourse() {
    this.common.display.loader = true;
    this.api.addCourse(this.course)
      .subscribe(course => {
        this.modal.close();
        this.common.display.loader = false;
        swal(
          'Good job!',
          'New course has been add!',
          'success'
        );
        this.clearData();
        console.log(course);
        this.getCourses();
      });
  }

  /* Delete Course */
  deletecourse(id) {
    this.common.display.loader = true;
    this.api.deleteCourse(id)
      .subscribe(res => {
        this.common.display.loader = false;
        swal(
          'Good job!',
          'Course has been deleted!',
          'success'
        );
        this.getCourses();
      });
  }

  /* Edit Course */
  editCourse(course, addCourseModal) {
    this.course = course;
    this.courseModalOpen(addCourseModal);
  }

  /* Update Course */
  updateCourse() {
    this.common.display.loader = true;
    this.api.updateCourse(this.course._id, this.course)
      .subscribe(res => {
        this.modal.close();
        this.common.display.loader = false;
        swal(
          'Good job!',
          'Course has been updated!',
          'success'
        );
        this.clearData();
        this.getCourses();
      })
  }

  /* Clear */
  clearData() {
    this.course = {
      _id: '',
      name: '',
      duration: 0,
      fee: 0,
      seats: 0,
      firstYearFilledSeats: 0
    };
  }


  courseModalOpen(addCourseModal) {
    this.modal = this.modalService.open(addCourseModal);
  }
}

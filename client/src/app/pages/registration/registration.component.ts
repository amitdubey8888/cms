import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../providers/common.service';
import { ApiService } from '../../providers/api.service';
import { Student } from '../../models/student';
import { Course } from '../../models/course';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  students: Student[];
  courses: Course[];
  student: Student = {
    _id: '',
    firstName: '',
    lastName: '',
    fatherName: '',
    motherName: '',
    gender: '',
    category: '',
    subCategory: '',
    domicile: '',
    religion: '',
    dob: new Date(),
    mobile: '',
    email: '',
    state: '',
    address: '',
    district: '',
    adhar: '',
    fatherMobile: '',
    fatherProfession: '',
    academicDetails: [
      {
        exam: '',
        rollNumber: '',
        passingYear: '',
        marksObtain: '',
        totalMarks: '',
        subjects: '',
        medium: ''
      },
      {
        exam: '',
        rollNumber: '',
        passingYear: '',
        marksObtain: '',
        totalMarks: '',
        subjects: '',
        medium: ''
      }
    ],
    course: [],
    subjects: [],
    year: '',
    courseFee: {
      firstYear: 0,
      secondYear: 0,
      thirdYear: 0
    },
    studentPay: {
      firstYear: 0,
      secondYear: 0,
      thirdYear: 0
    },
    photo: '',
    signature: '',
  };
  subjects: String = '';


  constructor(
    private common: CommonService,
    private api: ApiService
  ) {
    this.common.display.navbar = true;
    this.common.display.footer = true;
    this.common.display.copyright = true;
  }

  ngOnInit() {
    this.getCourses();
  }

  getCourses() {
    this.common.display.loader = true;
    this.api.getAll('course')
      .subscribe(courses => {
        console.log(courses);
        this.common.display.loader = false;
        this.courses = courses;
      });
  }

  addNewStudent() {
    this.common.display.loader = true;
    this.student.subjects = this.subjects.split(',');
    console.log(this.student);
    this.api.addOne('student', this.student)
      .subscribe(res => {
        this.common.display.loader = true;
        console.log(res);
      });
  }

  updateStudentDetails() {
    console.log(this.student);
  }

}

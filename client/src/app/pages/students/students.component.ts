import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../providers/common.service';
import { ApiService } from '../../providers/api.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  private students = [
    {
      name: 'Jay Rana',
      fatherName: 'Ram Avatar Singh Rana',
      motherName: 'Kamlesh Devi',
      dob: new Date(),
      mobile: '+918385803337',
      email: 'jkrana008@gmail.com',
      class: 'B. Sc.',
      year: '2nd',
      rollNumber: '1234',
      admissionDate: new Date()
    },
    {
      name: 'Jay Rana',
      fatherName: 'Ram Avatar Singh Rana',
      motherName: 'Kamlesh Devi',
      dob: new Date(),
      mobile: '+918385803337',
      email: 'jkrana008@gmail.com',
      class: 'B. Sc.',
      year: '2nd',
      rollNumber: '1234',
      admissionDate: new Date()
    }
  ];

  constructor(
    private common: CommonService,
    private api : ApiService
  ) {
    this.common.display.navbar = true;
    this.common.display.footer = true;
    this.common.display.copyright = true;
    this.getStudents();
  }

  ngOnInit() {
  }

  getStudents(){
    this.api.getAll('students')
      .subscribe(res=>{
        console.log(res);
        
      })
  }

}

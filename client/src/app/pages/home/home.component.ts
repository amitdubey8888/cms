import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../providers/common.service';
import { ApiService } from '../../providers/api.service';
import { Course } from '../../models/course';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  courses: Course[];

  constructor(
    private common: CommonService,
    private api: ApiService
  ) {
    this.common.display.navbar = true;
    this.common.display.footer = true;
    this.common.display.copyright = true;
    this.getCourse();
    
  }

  ngOnInit() {
  }

  /* Get Courses */
  getCourse() {
    this.common.display.loader = true;
    this.api.getCourse()
      .subscribe(courses => {
    this.common.display.loader = false;        
        this.courses = courses;
      });
  }



}

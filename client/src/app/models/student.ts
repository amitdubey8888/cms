export class Student {
    _id?: string;
    firstName: string;
    lastName: string;
    fatherName: string;
    motherName: string;
    gender: string;
    category: string;
    subCategory: string;
    domicile: string;
    religion: string;
    dob: Date;
    mobile: string;
    email: string;
    state: string;
    address: string;
    district: string;
    adhar: string;
    fatherMobile: string;
    fatherProfession: string;
    academicDetails: any;
    course: any;
    subjects: any;
    year: string;
    courseFee: Object = {
        firstYear: 0,
        secondYear: 0,
        thirdYear: 0
    };
    studentPay: Object = {
        firstYear: 0,
        secondYear: 0,
        thirdYear: 0
    };
    photo: string;
    signature: string;
}

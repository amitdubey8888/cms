import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from './pages/home/home.component';
import {LoginComponent} from './pages/login/login.component';
import {RegistrationComponent} from './pages/registration/registration.component';
import {StudentsComponent} from './pages/students/students.component';
import {FeeComponent} from './pages/fee/fee.component';
import {CoursesComponent} from './pages/courses/courses.component';
import { StudentDetailsComponent } from './pages/student-details/student-details.component';
import { FeeDetailsComponent } from './pages/fee-details/fee-details.component';


const appRoutes:Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'students', component: StudentsComponent},
  {path: 'student-details', component: StudentDetailsComponent},
  {path: 'fee', component: FeeComponent},
  {path: 'fee-details', component: FeeDetailsComponent},
  {path: 'courses', component: CoursesComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: '**', redirectTo: '/login'}
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}

import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Course } from '../models/course';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {

  private URL = 'http://localhost:3000/api/';
  // private URL = 'http://34.210.223.78:3000/api/'; 
  headers = new Headers();

  constructor(private http: Http) { }

  /* Common API's */

  // GET All Records
  getAll(subURL) {
    return this.http.get(this.URL + subURL)
      .map(res => res.json());
  }

  // Add One Record
  addOne(subURL, newRecord) {
    this.headers.append('Content-Type', 'application/json');
    return this.http.post(this.URL + subURL, newRecord, { headers: this.headers })
      .map(res => res.json());
  }

  // Update One Record
  updateOne(subURL, id, record) {
    this.headers.append('Content-Type', 'application/json');
    return this.http.put(this.URL + subURL + '/' + id, record, { headers: this.headers })
      .map(res => res.json());
  }

  // Delete One Record
  deleteOne(subURL, id) {
    return this.http.delete(this.URL + subURL + '/' + id)
      .map(res => res.json());
  }

  /* End of Common API's */

  // Retrieving Courses
  getCourse() {
    return this.http.get(this.URL + 'course')
      .map(res => res.json());
  }

  // Add Course
  addCourse(newCourse) {
    this.headers.append('Content-Type', 'application/json');
    return this.http.post(this.URL + 'course', newCourse, { headers: this.headers })
      .map(res => res.json());
  }

  // Delete Course
  deleteCourse(id) {
    return this.http.delete(this.URL + 'course/' + id)
      .map(res => res.json());
  }

  // Update Course
  updateCourse(id, course) {
    this.headers.append('Content-Type', 'application/json');
    return this.http.put(this.URL + 'course/' + id, course, { headers: this.headers })
      .map(res => res.json());
  }

}

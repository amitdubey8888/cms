import {Injectable} from '@angular/core';

@Injectable()
export class CommonService {

  public loginUser = null;
  public display = {
    navbar: false,
    footer: false,
    copyright: false,
    loader: false
  };
 

  constructor() {
  }

}

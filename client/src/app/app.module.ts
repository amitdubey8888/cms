import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {APP_BASE_HREF} from "@angular/common";


import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LoadersCssModule } from 'angular2-loaders-css';
import swal from 'sweetalert2';

import { CommonService } from './providers/common.service';
import {ApiService} from './providers/api.service';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app.routing';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { StudentsComponent } from './pages/students/students.component';
import { FeeComponent } from './pages/fee/fee.component';
import { CoursesComponent } from './pages/courses/courses.component';
import { StudentDetailsComponent } from './pages/student-details/student-details.component';
import { FeeDetailsComponent } from './pages/fee-details/fee-details.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegistrationComponent,
    StudentsComponent,
    FeeComponent,
    CoursesComponent,
    StudentDetailsComponent,
    FeeDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    LoadersCssModule,
    NgbModule.forRoot()
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/'},
    CommonService,
    ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }

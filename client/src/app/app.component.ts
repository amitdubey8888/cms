import { Component } from '@angular/core';
import swal from 'sweetalert2'

import { CommonService } from './providers/common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  display: any;

  constructor(
    private common: CommonService
  ) {
  }

  ngOnInit() {
    this.display = this.common.display;
  }
}
